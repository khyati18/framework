# CDLI Framework Development CLI

`cdlidev.py` is a simple Python interface to [Docker](https://docs.docker.com/) & [Docker Compose](https://docs.docker.com/compose/) which streamlines the process of bringing up a development environment for CDLI Framework apps.

While this tool handles the basic process of launching, updating, and shutting down a set of apps and infrastructure containers and data volumes, some knowledge of Docker and Compose will be useful.


### Features

The CLI is most simply a wrapper around the command `docker-compose --project-name cdlidev ...` and its most common commands (`up` and `down`). In fact, any Docker Compose commands and arguments may be passed through it. It offers, however, a few useful additional features not easily available through the Compose command, such as:

  * A configuration file (`config.json`) which allows enabling/disabling of specific app containers (via `--scale SERVICE=NUM`).
  * A short-hand command (`destroy`) to delete all persistence volumes which mimics `docker-compose down --volumes` but with additional user confirmation.


### Requirements & Support

Using this CLI requires Python, Docker, and Docker Compose. The required Python dependencies (Six and Docopt) should be available to any system with Docker Compose installed via Pip (`pip install docker-compose`) or may be installed independently.

  * OSX with [Docker for Mac](https://www.docker.com/docker-mac) is fully supported.
  * Linux should currently work *(many OS-supplied packages are quite old, so please follow the instructions for an up-to-date version from the [Docker CE download page](https://www.docker.com/community-edition#/download))*.
    * __Important__: Linux users will need to create a local user with UID of 2354 (mirroring the "cdli" user inside the app containers). This user should be given write permissions on the repository folder. An easy solution is the command: `setfacl -R -m default:user:2354:rwX,user:2354:rwX [framework_folder]`, which sets up extended ACL permissions on the folder without requiring and changes to its's owner, group, or mode.
  * [Docker CE for Windows](https://store.docker.com/editions/community/docker-ce-desktop-windows) will probably require some additional tweaking.


### Usage & Configuration

Run `./cdlidev.py --help` or look at the docstring at the top of the file for a full set of commands and arguments.

In addition to command line parameters, the app loads required configuration information from `config.json`. Since this file may contain sensitive information, it is ignored by the git repository. An included `config.json.dist` file should be copied to `config.json` and customized as necessary to enable apps and supply development secrets.

The development stack currently launches the following services:
  * __Nginx__, available to the host at http://127.0.0.1:2354
  * __phpMyAdmin__, available to the host at http://127.0.0.1:2355
  * __MariaDB__, available internally to containers at tcp://mariadb:3306
  * __Redis__, available internally to containers at tcp://redis:6379


### Direct Container Access

It is likely that at some point you will need to directly access a running container to copy files and run commands. This can be accomplished using the `docker` command.

* Docker compose names containers following the scheme `cdlidev_[app_][name]_1`. For example, MariaDB will be named `cdlidev_mariadb_1` and the search app named `cdlidev_app_search_1`.
* Since container images provide a single, stripped-down Linux environment, not all commands are available inside a container's shell. Basic commands (`sh`, `ls`, `cp`, etc.) are usually available, as well as executables provided by the container itself (such as `mysql`). Many containers --- especially apps --- are based on the minimalist Alpine Linux and use [BusyBox](https://busybox.net/downloads/BusyBox.html) to provide a nearly complete suite of Linux shell commands.


##### Container Command Examples

* To start a live shell in a container: `docker exec -ti [container_name] sh`
* To copy a file to a container's `/tmp` folder: `docker cp [local_path] [container_name]:[container_path]`


### Database Imports & Permissions

The MariaDB database is initially set up empty with no `root` password. This allows you immediate access through the PhpMyAdmin interface. However, PhpMyAdmin is not suitable for importing huge database dumps. The best way to accomplish this is something like the following:

```
# In your shell
docker cp cdlidb.sql.gz cdlidev_mariadb_1:/tmp/cdlidb.sql.gz
docker exec -ti cdlidev_mariadb_1 bash

# In the container shell
zcat /tmp/cdlidb.sql.gz | mysql -u root
rm /tmp/cdlidb.sql.gz
```
*(The above command assumes a gzipped dump that includes the proper SQL statements to create the database itself.)*

Since database permissions are also not set-up by default, you will need to add them using PhpMyAdmin or the MySQL command. The following queries demonstrate the pattern followed by two existing apps:

```
CREATE USER 'app_search'@'cdlidev_app_search_1.cdlidev_nodelocal-private';
GRANT ALL ON cdlidb.* TO 'app_search'@'cdlidev_app_search_1.cdlidev_nodelocal-private';

CREATE USER 'app_drupal7'@'cdlidev_app_drupal7_1.cdlidev_nodelocal-private';
GRANT ALL ON drupal7.* TO 'app_drupal7'@'cdlidev_app_drupal7_1.cdlidev_nodelocal-private';
```


### Development, Contribution, Bugs

* The CLI was developed primarily by Tim Bellefleur (@nomoon) on OSX using Python 3.6, Docker 17.12, and Compose 1.19.
* Issues and pull requests to improve the tool are graciously welcomed.


# CDLI Front-end Development

CDLI uses [SASS](https://sass-lang.com/) to write clean and composable CSS. You can learn more about how to get the most out of SASS [here](https://sass-lang.com/guide). 


### Setup

In order to transpile SASS into CSS correctly, we use LibSass, implemented in the form of Node-Sass. Additionally, we use Autoprefixer plugin of PostCSS to add ventor prefixes like `-webkit-` and `-moz-` to CSS properties which were experimental in the earlier versions of the browsers. These tools allow us to use Bootstrap 4 SCSS.

We need [Node.js and NPM](https://nodejs.org/en/download/) for this. Download it from the official website or `sudo apt install npm`. 

Install the reuqired packages with this command. Since these packages are being installed globally, no `node_modules` folder or `packages.json` file should be created.

```
npm install --global npx node-sass postcss-cli autoprefixer
```

Then open Windows Powershell or Teminal in the `framework/dev` directory. Use our wrapper `sass-cdli.py` to use the Node packages to turn SCSS into prefixed CSS. Windows Powershell users should not use shorthand `./sass-cdli.py` but rather use this:

```
cd framework/dev
python sass-cdli.py
```

See `python sass-cdli.py --help` for more options to this script.

This will watch the `framework/dev/assets/sass` folder for any changes and then transpile the code into CSS in `framework/webroot/assets/css`. Autoprefixer will not continuously watch, but would get executed only once before the script ends.



### Structure

The SASS files are located in `framework/dev/assets/sass` and have the end with the `.scss` filetype.

The `main.scss` file includes general styles that apply universally to all pages. The `/components` folder houses styles for parts of the websites that will be repeated, such as the navigation bar. This way, we can build components whose styles do not have to be repeated and can be expanded upon if specific pages require something different. The `/pages` folder includes styles specific to a webpage, for example the homepage. The `single.scss` file is for object entries.
