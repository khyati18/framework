<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Month $month
 */
?>
<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <div class="capital-heading"><?= __('View Month') ?></div>

        <table class="table-bootstrap">
            <tbody>
                <tr>
                    <th scope="row"><?= __('Month No') ?></th>
                    <td><?= h($month->month_no) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Id') ?></th>
                    <td><?= $this->Number->format($month->id) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Sequence') ?></th>
                    <td><?= $this->Number->format($month->sequence) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Composite Month Name') ?></th>
                    <td><?= $this->Text->autoParagraph(h($month->composite_month_name)); ?></td>
                </tr>
            </tbody>
        </table>

    </div>

    <div class="col-lg boxed">
        <div class="capital-heading"><?= __('Related Actions') ?></div>
        <?= $this->Html->link(__('Edit Month'), ['action' => 'edit', $month->id], ['class' => 'btn-action']) ?>
        <?= $this->Form->postLink(__('Delete Month'), ['action' => 'delete', $month->id], ['confirm' => __('Are you sure you want to delete # {0}?', $month->id), 'class' => 'btn-action']) ?>
        <?= $this->Html->link(__('List Months'), ['action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Month'), ['action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Dates'), ['controller' => 'Dates', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Date'), ['controller' => 'Dates', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
    </div>

</div>


<div class="boxed mx-0">
    <?php if (empty($month->dates)): ?>
        <div class="capital-heading"><?= __('No Related Dates') ?></div>
    <?php else: ?>
        <div class="capital-heading"><?= __('Related Dates') ?></div>
        <table cellpadding="0" cellspacing="0" class="table-bootstrap">
            <thead>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Day No') ?></th>
                <th scope="col"><?= __('Day Remarks') ?></th>
                <th scope="col"><?= __('Month Id') ?></th>
                <th scope="col"><?= __('Is Uncertain') ?></th>
                <th scope="col"><?= __('Month No') ?></th>
                <th scope="col"><?= __('Year Id') ?></th>
                <th scope="col"><?= __('Dynasty Id') ?></th>
                <th scope="col"><?= __('Ruler Id') ?></th>
                <th scope="col"><?= __('Absolute Year') ?></th>
                <th scope="col"><?= __('Actions') ?></th>
            </thead>
            <tbody>
                <?php foreach ($month->dates as $dates): ?>
                <tr>
                    <td><?= h($dates->id) ?></td>
                    <td><?= h($dates->day_no) ?></td>
                    <td><?= h($dates->day_remarks) ?></td>
                    <td><?= h($dates->month_id) ?></td>
                    <td><?= h($dates->is_uncertain) ?></td>
                    <td><?= h($dates->month_no) ?></td>
                    <td><?= h($dates->year_id) ?></td>
                    <td><?= h($dates->dynasty_id) ?></td>
                    <td><?= h($dates->ruler_id) ?></td>
                    <td><?= h($dates->absolute_year) ?></td>
                    <td class="d-flex flex-row">
                        <?= $this->Html->link(
                            $this->Html->tag('i', '', ['class' => 'fa fa-search']),
                            ['controller' => 'Dates', 'action' => 'view', $dates->id],
                            ['escape' => false, 'class' => 'btn btn-outline-primary m-1', 'title' => 'View']) ?>
                        <?= $this->Html->link(
                            $this->Html->tag('i', '', ['class' => 'fa fa-edit']),
                            ['controller' => 'Dates', 'action' => 'edit', $dates->id],
                            ['escape' => false, 'class' => 'btn btn-outline-success m-1', 'title' => 'Edit']) ?>
                        <?= $this->Form->postLink(
                            $this->Html->tag('i', '', ['class' => 'fa fa-trash']),
                            ['controller' => 'Dates', 'action' => 'delete', $dates->id],
                            ['confirm' => __('Are you sure you want to delete # {0}?', $dates->id), 'escape' => false, 'class' => 'btn btn-outline-danger m-1', 'title' => 'Delete']) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php endif; ?>
</div>


