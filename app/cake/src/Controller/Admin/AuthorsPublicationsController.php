<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * AuthorsPublications Controller
 *
 * @property \App\Model\Table\AuthorsPublicationsTable $AuthorsPublications
 *
 * @method \App\Model\Entity\AuthorsPublication[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class AuthorsPublicationsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Publications', 'Authors']
        ];
        $authorsPublications = $this->paginate($this->AuthorsPublications);

        $this->set(compact('authorsPublications'));
    }

    /**
     * View method
     *
     * @param string|null $id Authors Publication id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $authorsPublication = $this->AuthorsPublications->get($id, [
            'contain' => ['Publications', 'Authors']
        ]);

        $this->set('authorsPublication', $authorsPublication);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $authorsPublication = $this->AuthorsPublications->newEntity();
        if ($this->request->is('post')) {
            $authorsPublication = $this->AuthorsPublications->patchEntity($authorsPublication, $this->request->getData());
            if ($this->AuthorsPublications->save($authorsPublication)) {
                $this->Flash->success(__('The authors publication has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The authors publication could not be saved. Please, try again.'));
        }
        $publications = $this->AuthorsPublications->Publications->find('list', ['limit' => 200]);
        $authors = $this->AuthorsPublications->Authors->find('list', ['limit' => 200]);
        $this->set(compact('authorsPublication', 'publications', 'authors'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Authors Publication id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $authorsPublication = $this->AuthorsPublications->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $authorsPublication = $this->AuthorsPublications->patchEntity($authorsPublication, $this->request->getData());
            if ($this->AuthorsPublications->save($authorsPublication)) {
                $this->Flash->success(__('The authors publication has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The authors publication could not be saved. Please, try again.'));
        }
        $publications = $this->AuthorsPublications->Publications->find('list', ['limit' => 200]);
        $authors = $this->AuthorsPublications->Authors->find('list', ['limit' => 200]);
        $this->set(compact('authorsPublication', 'publications', 'authors'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Authors Publication id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $authorsPublication = $this->AuthorsPublications->get($id);
        if ($this->AuthorsPublications->delete($authorsPublication)) {
            $this->Flash->success(__('The authors publication has been deleted.'));
        } else {
            $this->Flash->error(__('The authors publication could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
